# vim: set fileencoding=utf-8:

def readToList(filename):
    return [l.strip() for l in open(filename).readlines()]

from setuptools import setup

setup(
        name='couchbaselite',
        packages=['couchbaselite'],
        version="0.1.dev2",
        install_requires=['jsondate', 'requests']
        )
