class View(object):
    def __init__(self, result):
        self.offset = result['offset']
        self.total = result['total_rows']
        self.rows = result['rows']
        if len(self.rows) == 1:
            self.result = self.rows[0]
            if self.result.has_key('value'):
                self.value = self.result['value']
        else:
            self.result = None

    def __iter__(self):
        for row in self.rows:
            yield(row)
