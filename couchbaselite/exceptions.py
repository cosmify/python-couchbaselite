class CouchbaseError(Exception):
    def __init__(self, **params):
        pass

class KeyExistsError(CouchbaseError):
    pass

class NotFoundError(CouchbaseError):
    pass

class NotStoredError(CouchbaseError):
    pass
