# Copyright (c) 2016 by Cosmify, Inc.  All rights reserved.
# 
# See:  LICENSE.txt for complete licensing information.


from couchbaselite.bucket_manager import BucketManager
from couchbaselite.conn import Conn
from couchbaselite.exceptions import KeyExistsError
from couchbaselite.exceptions import NotFoundError
from couchbaselite.exceptions import NotStoredError
from couchbaselite.result import Result
from couchbaselite.result import ValueResult
from couchbaselite.result import val_label
from urlparse import urlparse
import re

valid_bucket_name_regex = re.compile('[a-z0-9$_()+-/]+')

def parse_couchbase_url(url):
    parts = urlparse(url)
    host = None
    port = None
    if parts.scheme == 'couchbase':
        if parts.path == '':
            bucket = parts.netloc
        else:
            host = parts.hostname
            port = parts.port
            bucket = parts.path.lstrip("/")
    else:
        bucket = url

    return {'bucket' : bucket, 'host' : host, 'port' : port}

def v_payload(value):
    """
    Standardize how scalar values are wrapped in a dictionary.
    """
    return { val_label : value }

class Bucket(object):
    def __init__(self, url, host=None, port=None, **connkwargs):
        """
        Create a Bucket. The url can be a simple bucket name, or can be a
        couchbase:// style url. Any host and port arguments provided will
        override a host or port specified as part of a couchbase url.

        Any other keyword args will be passed to the `__init__` method for Conn.
        """
        info = parse_couchbase_url(url)
        name = info['bucket']
        host = host or info['host']
        port = port or info['port']
        assert valid_bucket_name_regex.match(name)
        self._name = name
        self._conn = Conn(host, port, **connkwargs)
        self._conn.create_db(name)

    def __unicode__(self):
        return u"<couchbaselite.bucket.Bucket '%s' @ %s>" % (self._name , self._conn.desc())

    def __str__(self):
            return unicode(self).encode('utf-8')

    def _path(self, key=None):
        if key:
            return "%s/%s" % (self._name, key)
        else:
            return self._name

    def _get(self, key, params=None):
        """
        Internal method which calls the corresponding conn method.
        """
        return self._conn.get(self._path(key), params=params)

    def _put(self, key, params=None, rev=None):
        """
        Internal method which calls the corresponding conn method.
        """
        return self._conn.put(self._path(key), params, rev)

    def _post(self, params=None, resourcePath = None):
        """
        Internal method which calls the corresponding conn method.
        """
        return self._conn.post(self._path(resourcePath), params) if resourcePath else self._conn.post(self._path(), params)

    def _delete(self, key, rev=None):
        """
        Internal method which calls the corresponding conn method.
        """
        return self._conn.delete(self._path(key), rev=rev)

    def _existing_rev(self, key):
        """
        If a key is present, return its latest revision ID.
        """
        both = self._existing_and_rev(key)
        if both:
            return both[1]

    def _existing_and_rev(self, key):
        """
        If a key is present, return the record and its latest revision ID.
        """
        if key is None:
            return
        existing = self._get(key)
        if existing.get('status') != 404:
            return existing, existing.get('_rev')

    def _upsert(self, key, value, rev):
        """
        Internal method which performs an upsert.
        """
        if isinstance(value, dict):
            payload = value
        else:
            payload = v_payload(value)

        if key is None:
            result = self._post(params=payload)
        else:
            result = self._put(key, payload, rev)

        return Result(result)


    def _compact(self):
        """
        Internal method for compacting the database.
        """
        return Result(self._post(params = dict(), resourcePath = u'_compact'))


    def upsert(self, key, value, rev=None):
        """
        Unconditionally store the object.
        """
        if rev is None:
            rev = self._existing_rev(key)
        return self._upsert(key, value, rev)

    def insert(self, key, value):
        """
        Store an object unless it already exists.
        """
        rev = self._existing_rev(key)
        if rev is not None:
            raise KeyExistsError(key=key)
        return self._upsert(key, value, rev)

    def replace(self, key, value):
        """
        Store an object only if it already exists.
        """
        rev = self._existing_rev(key)
        if rev is None:
            raise NotFoundError()
        return self._upsert(key, value, rev)

    def get(self, key, params=None, quiet=None):
        """
        Retrieve an object. If not found, raises a NotFoundError or returns
        None if in quiet mode.
        """
        result = self._get(key, params)
        if result.get('status') == 404:
            if not quiet:
                raise NotFoundError(key=key)
        else:
            return ValueResult(result)

    def append(self, key, value):
        """
        Append the value to the end of the existing record.
        """
        both = self._existing_and_rev(key)
        if both is None:
            raise NotStoredError(key=key)
        existing, rev = both
        new = u"%s%s" % (existing[val_label], value)
        return self._upsert(key, new, rev)

    def prepend(self, key, value):
        """
        Prepend the value to the beginning of the existing record.
        """
        both = self._existing_and_rev(key)
        if both is None:
            raise NotStoredError(key=key)
        existing, rev = both
        new = u"%s%s" % (value, existing[val_label])
        return self._upsert(key, new, rev)

    def remove(self, key, quiet=None):
        """
        Remove the record corresponding to the key.
        """
        rev = self._existing_rev(key)
        if rev is None:
            if not quiet:
                raise NotFoundError()
        else:
            result = self._delete(key, rev)
            return Result(result)

    def all_docs(self):
        result = self._conn.get(self._path('_all_docs'))
        return result.get('rows', [])

    def bucket_manager(self):
        """
        Returns the bucket manager for the bucket.
        """
        return BucketManager(self._name, self._conn)

    def temp_view(self, ddoc):
        return self.bucket_manager()._temp_view(ddoc)

    def query(self, design, view, **params):
        """
        Returns the results provided by the specified view in the design document.
        """
        use_devmode = params.get('use_devmode', False)
        if params.has_key('use_devmode'):
            del params['use_devmode']
        return self.bucket_manager()._view(design, view, use_devmode, params)


    ### 20160320 - EC:
    def compact(self):
        """
        Compact the database for smaller footprint and more efficient performance.
        """
        return self._compact()

