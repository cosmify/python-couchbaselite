from couchbaselite.view import View

class BucketManager(object):
    """
    Bucket managers should be created by calling the `bucket_manager()` method
    on an existing bucket.
    """
    def __init__(self, name, conn):
        self._bucket_name = name
        self._conn = conn

    def _path(self, ddoc_name):
        return "%s/_design/%s" % (self._bucket_name, ddoc_name)

    def _payload(self, ddoc):
        return {
                'language' : 'javascript',
                'views' :  ddoc
                }

    def _design_rev(self, name, use_devmode=True):
        if use_devmode:
            name = "dev_%s" % name
        result = self._conn.get(self._path(name))
        return result.get('_rev')

    def design_create(self, name, ddoc, use_devmode=True):
        """
        Creates or updates a design document.
        """
        rev = self._design_rev(name, use_devmode=use_devmode)
        if use_devmode:
            name = "dev_%s" % name
        payload = self._payload(ddoc)
        result = self._conn.put(self._path(name), payload, rev)
        assert result.get('ok'), "result was %s" % result

    def design_get(self, name, use_devmode=True):
        if use_devmode:
            name = "dev_%s" % name
        result = self._conn.get(self._path(name))

        if result.get('status') == 404:
            return
        else:
            del result['_rev']
            del result['_id']
            return result

    def design_publish(self, name, ddoc=None):
        if ddoc is None:
            ddoc = self.design_get(name, use_devmode=True)
            if ddoc is None:
                raise Exception("can't find existing ddoc %s" % name)
            ddoc = ddoc['views']
        self.design_create(name, ddoc, use_devmode=False)

    def design_delete(self, name, use_devmode=True):
        rev = self._design_rev(name, use_devmode=use_devmode)
        if use_devmode:
            name = "dev_%s" % name
        result = self._conn.delete(self._path(name), rev)
        assert result.get('ok'), "result was %s" % result

    def _view_path(self, ddoc_name, view_name):
        return "%s/_view/%s" % (self._path(ddoc_name), view_name)

    def _view(self, ddoc_name, view, use_devmode, params):
        if use_devmode:
            ddoc_name = "dev_%s" % ddoc_name
        result = self._conn.get(self._view_path(ddoc_name, view), params=params)
        if result.get('status') == 404:
            raise Exception("%s:%s view not found" % (ddoc_name, view))
        else:
            return View(result)

    def _temp_view(self, ddoc):
        result = self._conn.post(self._path("_temp_view"), self._payload(ddoc))
        return result
