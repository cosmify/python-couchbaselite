val_label = 'value'

class Result(object):
    def __init__(self, result):
        self._result = result
        self.rc = result.get('status')
        self.success = result.get('ok', False)
        self.key = result.get('id')
        self.rev = result.get('rev')

class OperationResult(Result):
    pass

class ValueResult(OperationResult):
    def __init__(self, result):
        super(ValueResult, self).__init__(result)
        if result.has_key(val_label):
            self.value = result.get(val_label)
        else:
            self.value = result
        self.success = self.value is not None

    def __getattr__(self, key):
        return self._result.get(key)

    def __getitem__(self, key):
        return self._result.get(key)
