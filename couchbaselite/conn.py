# Copyright (c) 2016 by Cosmify, Inc.  All rights reserved.
# 
# See:  LICENSE.txt for complete licensing information.


from time import sleep
import inspect
import jsondate as json
import logging
import re
import requests

BASELOGGER = 'couchbaselite'
DEFAULT_HOST = '127.0.0.1'
DEFAULT_PORT = 59840
LOG_FORMAT = '%(asctime)s %(levelname)s %(message)s'

class CouchliteUnavailable(Exception):
    pass

class Conn(object):
    def _null_log(self):
        self._log = logging.getLogger(BASELOGGER)
        for h in self._log.handlers:
            self._log.removeHandler(h)
        self._log.addHandler(logging.NullHandler())

    def _configure_log(self, logfile):
        self._log = logging.getLogger(BASELOGGER)
        self._log.setLevel(logging.DEBUG)
        formatter = logging.Formatter(fmt=LOG_FORMAT)
        handler = logging.FileHandler(logfile)
        handler.setFormatter(formatter)
        self._log.addHandler(handler)

    def _configure_stream_log(self):
        self._log = logging.getLogger(BASELOGGER)
        self._log.setLevel(logging.DEBUG)
        formatter = logging.Formatter(fmt=LOG_FORMAT)
        handler = logging.StreamHandler()
        handler.setFormatter(formatter)
        self._log.addHandler(handler)

    def __init__(self, host=None, port=None, max_wait_secs=0, wait_incr=0.2, log=None):
        """
        Raises a `CouchliteUnavailable` error if database is unreachable, unless
        max_wait_secs is greater than zero, in which case it tries every
        `wait_incr` seconds until `max_wait_secs` is reached, and raises a
        `CouchliteUnavailable` error if database has not been reached in that time.

        If a `log` parameter is passed, logging is enabled. `log` may be a Logger
        instance, or a string defining the name of a file to log to, or True in
        which case logs are send to `sys.stderr`.
        """
        self._host = host or DEFAULT_HOST
        self._port = port or DEFAULT_PORT

        if log is None:
            self._null_log()
        elif isinstance(log, basestring):
            self._configure_log(log)
        elif isinstance(log, logging.Logger):
            self._log = log
        elif isinstance(log, bool) and log:
            self._configure_stream_log()
        else:
            raise Exception("log parameter should be a Logger instance or filename to log to")

        if max_wait_secs:
            self._wait_until_up(max_wait_secs, wait_incr)
        else:
            if not self._check():
                raise CouchliteUnavailable

    def desc(self):
        return "%s:%s" % (self._host, self._port)

    def _check(self):
        """
        Returns a boolean indicating whether the server can be contacted.
        """
        try:
            source = inspect.stack()[-1][1]
            self._log.debug("attempting to contact couchlite server from %s..." % source)
            self.get("/")
            self._log.debug("...success contacting couchlite from %s!" % source)
            return True
        except requests.exceptions.ConnectionError:
            self._log.debug("...failed")
            return False

    def _wait_until_up(self, max_wait_secs, wait_incr):
        """
        Waits up to max_wait_secs, attempting to contact the server every
        wait_incr. Returns when server has been contacted or throws a
        CouchliteUnavailable exception if limit is reached without success.
        """
        assert wait_incr > 0
        max_tries = int(float(max_wait_secs)/wait_incr)
        for i in xrange(max_tries):
            if self._check():
                break
            self._log.debug("  waiting %s secs before trying again" % wait_incr)
            sleep(wait_incr)
        if i >= (max_tries-1):
            raise CouchliteUnavailable()

    def path_to(self, *path):
        return "http://%s:%s/%s" % (self._host, self._port, "/".join(path))

    def get(self, path, params=None):
        response = requests.get(self.path_to(path), params=params)
        return response.json()

    def put(self, path, data=None, rev=None):
        if data:
            data = json.dumps(data)
        else:
            data = None

        if rev:
            headers = { 'If-Match' : rev }
        else:
            headers = {}

        response = requests.put(self.path_to(path), data=data, headers=headers)
        return response.json()

    def post(self, path, data=None):
        if data is not None:
            data = json.dumps(data)
        else:
            data = None
        response = requests.post(self.path_to(path), data=data)
        return response.json()

    def delete(self, path, rev=None):
        # passing rev in params, since If-Match header does not seem to work for delete
        response = requests.delete(self.path_to(path), params={'rev' : rev})
        return response.json()

    def all_dbs(self):
        return self.get("_all_dbs")

    def create_db(self, name):
        result = self.put(name)
        assert result.get('ok') or result['status'] == 412

    def remove_db(self, name):
        result = self.delete(name)
        assert result.get('ok')

    def remove_all_dbs(self, matching=None):
        for name in self.all_dbs():
            if matching is not None:
                if not re.match(matching, name):
                    continue
            self.remove_db(name)
