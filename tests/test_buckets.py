from couchbaselite.bucket import valid_bucket_name_regex
from couchbaselite.bucket import Bucket
from couchbaselite.bucket import parse_couchbase_url

def test_url_parsing_name():
    info = parse_couchbase_url("foo")
    assert info['bucket'] == 'foo'
    assert info['host'] == None
    assert info['port'] == None

def test_url_parsing_cb_bucket():
    info = parse_couchbase_url("couchbase://foo")
    assert info['bucket'] == 'foo'
    assert info['host'] == None
    assert info['port'] == None

def test_url_parsing_cb_bucket_with_host():
    info = parse_couchbase_url("couchbase://localhost/foo")
    assert info['bucket'] == 'foo'
    assert info['host'] == 'localhost'
    assert info['port'] == None

def test_url_parsing_cb_bucket_with_host_and_port():
    info = parse_couchbase_url("couchbase://localhost:9999/foo")
    assert info['bucket'] == 'foo'
    assert info['host'] == 'localhost'
    assert info['port'] == 9999

def test_valid_bucket_name_regexs():
    assert valid_bucket_name_regex.match("foo")
    assert valid_bucket_name_regex.match("foo123($)_+-/")

def test_invalid_bucket_name_regexs():
    assert not valid_bucket_name_regex.match("Foo")

def test_create_bucket():
    bucket = Bucket("foo")
    assert bucket._name == 'foo'

def test_all_docs_for_bucket():
    bucket = Bucket("foo")
    assert isinstance(bucket.all_docs(), list)

def test_get_nonexistent_key():
    bucket = Bucket("foo")
    assert bucket.get("notavalidkey99999", quiet=True) is None

def insert_payload_then_remove(payload):
    bucket = Bucket("foo")
    bucket.remove("bar", quiet=True)
    result = bucket.insert("bar", payload)
    assert result.success
    result = bucket.get("bar")
    if isinstance(payload, dict):
        for k, v in payload.iteritems():
            assert result[k] == v
    else:
        assert result.value == payload
    bucket.remove("bar")
    assert bucket.get("bar", quiet=True) is None

def test_insert_key_value_and_delete():
    payload = 123
    insert_payload_then_remove(payload)

def test_insert_list():
    payload = ["apple", "orange"]
    insert_payload_then_remove(payload)

def test_insert_dict():
    payload = { "a" : 1, "b" : 2 }
    insert_payload_then_remove(payload)

def test_insert_doc_without_key():
    bucket = Bucket("foo")
    result = bucket.insert(None, {"title" : "My Document", "author" : "Anonymous"})
    assert result.success
    key = result.key
    result = bucket.get(key)
    assert result.title == "My Document"
    assert result.author == "Anonymous"
    assert result['title'] == "My Document"
    assert result['author'] == "Anonymous"
    result = bucket.remove(key)
    assert result.success

def test_append():
    key = 'astr'
    bucket = Bucket("foo")
    bucket.remove(key, quiet=True)
    result = bucket.insert(key, "middle")
    assert result.success
    result = bucket.append(key, "end")
    assert result.success
    result = bucket.get(key)
    assert result.value == 'middleend'
    result = bucket.prepend(key, "begin")
    assert result.success
    result = bucket.get(key)
    assert result.value == 'beginmiddleend'
