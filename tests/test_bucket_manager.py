from couchbaselite.bucket import Bucket
from couchbaselite.view import View
import random

bucket = Bucket("foo")
manager = bucket.bucket_manager()

ddoc = {
            "noop" : {
                'map' : """function(doc) {
                    if (doc.type == 'randval') {
                        emit(doc)
                    }
                }"""
            },
            "countAll" : {
                'map' : """function(doc) {
                    if (doc.type == 'randval') {
                        emit(doc._id)
                    }
                }""",
                'reduce' : """function(keys, values, rereduce) {
                    return(values.length);
                }
                """
            }
        }

def test_populate_db():
    for i in xrange(100):
        bucket.insert(None, {"type" : "randval", "value" : random.random()})

def test_rev_of_new_ddoc():
    assert manager._design_rev("newddoc") is None

def test_create_design_document():
    assert manager.design_get("my_ddoc") is None
    manager.design_create("my_ddoc", ddoc)
    assert manager.design_get("my_ddoc")['views'].has_key('noop')
    manager.design_publish("my_ddoc")
    assert manager.design_get("my_ddoc", use_devmode=False)['views'].has_key('noop')
    # manager.design_delete("my_ddoc", use_devmode=False)
    # manager.design_delete("my_ddoc", use_devmode=True)

def test_iterate_noop_view():
    view = bucket.query("my_ddoc", "noop")
    assert isinstance(view, View)
    for row in view:
        assert row['key']['type'] == 'randval'

def test_iterate_count():
    view = bucket.query("my_ddoc", "countAll")
    assert view.value == 100

def test_iterate_count_with_params():
    view = bucket.query("my_ddoc", "countAll", use_devmode=False, limit=5)
    assert view.value == 5
