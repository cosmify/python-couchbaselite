from couchbaselite.conn import Conn
import uuid

exampleKey = str(uuid.uuid4())
examplePath = "foo/%s" % exampleKey
rev = None
prevRev = None

conn = Conn()
vendor = conn.get("/")['vendor']
print vendor['name'], vendor['version']

def test_path_to():
    assert conn.path_to() == "http://localhost:59840/"
    assert conn.path_to("foo") == "http://localhost:59840/foo"
    assert conn.path_to("foo", "bar") == "http://localhost:59840/foo/bar"
    assert conn.path_to("foo", "bar", "baz") == "http://localhost:59840/foo/bar/baz"

def test_all_dbs():
    assert isinstance(conn.all_dbs(), list)

def test_db_lifecycle():
    db_name = "lifecycle"
    assert not db_name in conn.all_dbs()
    conn.create_db(db_name)
    conn.create_db(db_name)
    assert db_name in conn.all_dbs()
    assert conn.get(db_name)['db_name'] == db_name
    conn.delete(db_name)
    assert not db_name in conn.all_dbs()

def test_cblite_err_no_rev():
    response = conn.put(examplePath, "invalid-payload-for-test")
    assert response['status'] == 502
    assert response.has_key('error')

def test_cblite_err_invalid_rev():
    response = conn.put(examplePath, "invalid-payload-for-test", rev="invalid")
    assert response['status'] == 502
    assert response.has_key('error')

def test_cblite_item_not_found():
    response = conn.get(examplePath)
    assert response['status'] == 404
    assert response['reason'] == 'missing'
    assert response['error'] == 'not_found'

def test_cblite_insert_new_item():
    global rev
    response = conn.put(examplePath, {"value" : "payload"})
    assert response['ok']
    assert response['id'] == exampleKey
    rev = response['rev']

def test_cblite_update_without_rev():
    response = conn.put(examplePath, {"value" : "newpayload"})
    assert response['status'] == 409
    assert response['error'] == 'conflict'

def test_cblite_update():
    global rev
    global prevRev
    response = conn.put(examplePath, {"value" : "newpayload"}, rev=rev)
    assert response['ok']
    assert response['id'] == exampleKey
    prevRev = rev
    rev = response['rev']

def test_cblite_update_prev_rev():
    response = conn.put(examplePath, {"value" : "newerpayload"}, rev=prevRev)
    assert response['status'] == 409
    assert response['error'] == 'conflict'

def test_cblite_update_new_rev():
    global rev
    global prevRev
    response = conn.put(examplePath, {"value" : "newerpayload"}, rev=rev)
    assert response['ok']
    assert response['id'] == exampleKey
    prevRev = rev
    rev = response['rev']

def test_cblite_get():
    response = conn.get(examplePath)
    assert response['_id'] == exampleKey
    assert response['_rev'] == rev
    assert response['value'] == 'newerpayload'

def test_cblite_delete_without_rev():
    response = conn.delete(examplePath)
    assert response['status'] == 409
    assert response['error'] == 'conflict'

def test_cblite_delete():
    global rev
    response = conn.delete(examplePath, rev=rev)
    assert response['ok']
    assert response['id'] == exampleKey
    rev = response['rev']

def test_cblite_item_deleted():
    response = conn.get(examplePath)
    assert response['status'] == 404
    assert response['reason'] == 'deleted'
    assert response['error'] == 'deleted'
